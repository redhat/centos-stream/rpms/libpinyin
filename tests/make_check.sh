#!/bin/bash

set -x

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

cd $1

VERSION=`rpmspec -q --srpm --qf "%{version}" libpinyin.spec 2>/dev/null`

if test -d libpinyin-$VERSION-build;
then cd libpinyin-$VERSION-build;
fi

cd libpinyin-$VERSION

./configure --prefix=/usr --with-dbm=KyotoCabinet --enable-libzhuyin
check_return_value $?
make
check_return_value $?
make check
exit $?
